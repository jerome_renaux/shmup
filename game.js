/**
 * Created by Jerome on 29-05-16.
 */
/*
 * Author: Jerome Renaux
 * E-mail: jerome.renaux@gmail.com
 */

var Game = {};

// Amount of pixels by which the bullet bodies must be adapted after changing frame
var bodyAdjusts = {
    missile:80,
    missile2:45,
    missile3:56,
    missile4:22,
    pellet:13
};

var Ship = function(type,time,life,x,y){
    Phaser.Sprite.call(this, game, -200, -200, 'spritesheet');
    this.type = type;
    this.time = time*Phaser.Timer.SECOND; // Time in seconds before the ship enters the scene
    this.life = life;
    this.mode = 0; // -1 = move left, 0 = move in, 1 = move right
    this.target = {x:x,y:y};
    var speed = 250;
    var frame = '';
    var delay = 1; // Time in seconds between two shots
    var missileFrame = 'missile2';
    var power = 15;
    switch(type){
        case 1:
            frame = 'ship2';
            break;
        case 2:
            frame = 'ship3';
            speed = 150;
            delay = 0.75;
            missileFrame = 'missile3';
            power = 30;
            break;
        case 3:
            frame = 'ship4';
            speed = 300;
            delay = 0.5;
            missileFrame = 'missile';
            power = 50;
    }
    this.lastShot = 0;
    this.shootDelay = delay*Phaser.Timer.SECOND;
    this.power = power;
    this.speed = speed;
    this.frameName = frame; // use frameName when using atlases
    this.missileFrame = missileFrame;
    this.exists = false;
    game.add.existing(this);
};
Ship.prototype = Object.create(Phaser.Sprite.prototype);
Ship.prototype.constructor = Ship;

// Sprite packing using https://www.leshylabs.com/apps/sstool/
Game.preload = function() {
    game.scale.pageAlignHorizontally = true;
    game.load.atlasJSONHash('spritesheet', 'assets/spritesheet.png', 'assets/sprites.json'); // Takes some time; don't add sprites immediately, do it in onCreate()
    game.load.image('stars','assets/back.jpg');
};

Game.create = function(){
    Game.stars = game.add.tileSprite(0, 0, 1080, 1920, 'stars');
    Game.stars.autoScroll(0,300);

    Game.player = game.add.sprite(350,550,'spritesheet','ship');
    game.physics.arcade.enable(Game.player);
    Game.player.life = 250;
    Game.player.power = 10;
    Game.player.missileFrame = 'missile4';
    Game.player.body.collideWorldBounds = true;
    Game.player.body.setSize(65,40,15,40);
    Game.player.lastShot = game.time.time;
    Game.player.shootDelay = Phaser.Timer.SECOND/4;

    Game.bullets = game.add.group();
    Game.bullets.enableBody = true;
    for(var i = 0; i < 70; i++) {
        Game.bullets.add(game.add.sprite(0,0,'spritesheet'));
    }
    Game.bullets.setAll('exists',false);
    Game.bullets.setAll('checkWorldBounds',true);
    Game.bullets.setAll('outOfBoundsKill',true);

    Game.start = game.time.time;

    Game.enemies = game.add.group();
    Game.enemies.enableBody = true;
    Game.enemies.add(new Ship(1,2,100,330,100));
    Game.enemies.add(new Ship(1,5,100,300,80));
    Game.enemies.add(new Ship(1,5,100,350,120));
    Game.enemies.add(new Ship(2,10,300,300,50));
    Game.enemies.add(new Ship(3,30,1000,300,50));
    Game.enemies.forEach(Game.shrinkBody,this);

    Game.explosions = game.add.group();
    for(var j = 0; j < 20; j++) {
        var xpl = game.add.sprite(0,0,'spritesheet','boom1');
        xpl.animations.add('explode', ['boom2','boom3','boom4','boom5','boom6','boom7'], 10).killOnComplete = true;
        Game.explosions.add(xpl);
    }
    Game.explosions.setAll('exists',false);

    Game.keys = game.input.keyboard.createCursorKeys();
    Game.keys.fire = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
};

Game.shrinkBody = function(enemy){
    enemy.body.setSize(90,50,15,0);
};

Game.shoot = function(agent){
    if(game.time.time - agent.lastShot < agent.shootDelay){return;}
    agent.lastShot = game.time.time;
    if(agent == Game.player){
        for(var k = 0; k < 2; k++) {
            Game.spawnBullet(agent,agent.missileFrame,agent.power,true,20*k,270);
        }
    }else{
        Game.spawnBullet(agent,agent.missileFrame,agent.power,false,0,90);
        if(agent.type == 3){
            for(var l = 0; l < 8; l++) {
                Game.spawnBullet(agent,'pellet',10,false,0,l*45);
            }
        }
    }
};

Game.spawnBullet = function(agent,frame,power,fromPlayer,x_offset,angle){
    var bullet = Game.bullets.getFirstExists(false);
    bullet.exists = true;
    bullet.fromPlayer = fromPlayer;
    bullet.frameName = frame;
    bullet.x = agent.x + 32 + x_offset;
    bullet.y = agent.y;
    bullet.power = power;
    bullet.body.setSize(bullet.width,bodyAdjusts[bullet.frameName],0,0);
    game.physics.arcade.velocityFromAngle(angle, 300, bullet.body.velocity);
};

Game.damage = function(_agent,_bullet){
    if(_agent == Game.player && _bullet.fromPlayer == true){return;}
    if(_agent != Game.player && _bullet.fromPlayer == false){return;}
    var xpl = Game.explosions.getFirstExists(false);
    xpl.exists = true;
    xpl.x = _bullet.x+10;
    xpl.y = _bullet.y + bodyAdjusts[_bullet.frameName]/2;
    _agent.life -= _bullet.power;
    if(_agent.life <= 0){
        _agent.kill();
    }
    _bullet.kill();
    xpl.animations.play('explode');
};

Game.update = function(){
    var speed = 250;
    var angle;

   if(Game.player.alive){game.physics.arcade.overlap(Game.player, Game.bullets,Game.damage);}
    game.physics.arcade.overlap(Game.enemies, Game.bullets,Game.damage);

    if (Game.keys.up.isDown && !Game.keys.right.isDown && !Game.keys.left.isDown) {
        angle = 90;
    }else if (Game.keys.up.isDown && Game.keys.left.isDown) {
        angle = 135;
    }else if (Game.keys.left.isDown && !Game.keys.up.isDown && !Game.keys.down.isDown) {
        angle = 180;
    }else if (Game.keys.left.isDown && Game.keys.down.isDown) {
        angle = 225;
    }else if (Game.keys.down.isDown && !Game.keys.right.isDown && !Game.keys.left.isDown) {
        angle = 270;
    }else if (Game.keys.down.isDown && Game.keys.right.isDown) {
        angle = 315;
    }else if (Game.keys.right.isDown && !Game.keys.down.isDown && !Game.keys.up.isDown) {
        angle = 0;
    }else if (Game.keys.up.isDown && Game.keys.right.isDown) {
        angle = 45;
    }
    angle = 360 - angle;
    game.physics.arcade.velocityFromAngle(angle, speed, Game.player.body.velocity);

    if(Game.keys.fire.isDown && Game.player.alive){
        Game.shoot(Game.player);
    }

    Game.enemies.forEach(function(ship){
        if (ship.alive && Game.start + ship.time < game.time.time) {
            ship.exists = true;
            if(Phaser.Math.distance(ship.x,ship.y,ship.target.x,ship.target.y) < 10){
                if(ship.mode == 0)
                {
                        ship.mode = -1;
                }else{
                    ship.mode *= -1;
                }
                if(ship.mode == -1){
                    ship.target.x = 50;
                }else if(ship.mode == 1){
                    ship.target.x = 600;
                }
            }
            game.physics.arcade.velocityFromAngle(Phaser.Math.radToDeg(Phaser.Math.angleBetween(ship.x,ship.y,ship.target.x,ship.target.y)), speed, ship.body.velocity);
        }
        if(ship.mode != 0){
            Game.shoot(ship);
        }
        if(!ship.alive){ship.destroy();}
    },this);
};

Game.shutdown = function(){

};

Game.render = function(){
    /*game.debug.body(Game.player);
    Game.bullets.forEachExists(function(bullet){
        game.debug.body(bullet);
    });
    Game.enemies.forEachExists(function(enemy){
        game.debug.body(enemy);
    });*/
};